package com.example.contest;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = ContestApplicationTests.class)
class ContestApplicationTests {

	@Test
	void contextLoads() {
	}

}
