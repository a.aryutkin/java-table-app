package com.example.contest.requests;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class EventEntryRequest {
    @JsonProperty("id")
    private long id;

    @JsonProperty("user_name")
    private String userName;

    @JsonProperty("organisation")
    private String organisation;
}
