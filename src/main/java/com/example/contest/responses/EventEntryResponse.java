package com.example.contest.responses;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Setter;
import lombok.NoArgsConstructor;

@Setter
@NoArgsConstructor
@AllArgsConstructor
public class EventEntryResponse {
    @JsonProperty("id")
    private long id;

    @JsonProperty("user_name")
    private String userName;

    @JsonProperty("organisation")
    private String organisation;

    @JsonProperty("created_at")
    private long createdAt;
}
