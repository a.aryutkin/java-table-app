package com.example.contest.entities;

import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import java.util.Date;

@Entity
@Data
@Table(name = "event_entry_small")
public class EventEntry {
	@Id
	@GeneratedValue
	private long id;

	@Column(name = "user_name", nullable = false)
	private String userName;

	@Column(name = "organisation", nullable = false)
	private String organisation;

	@Column(name = "created_at")
	@CreationTimestamp
	private Date createdAt;

	@ManyToOne
	@JoinColumn(name="event_id")
	private Event event;
}