package com.example.contest.entities;

import jakarta.persistence.*;

import java.util.List;

@Entity
@Table(name = "event_small")
public class Event {

	@Id
	@GeneratedValue
	private long id;

	@Column(name = "name", nullable = false)
	private String name;

	@OneToMany(mappedBy = "event")
	private List<EventEntry> eventEntries;
}