package com.example.contest.controllers;

import com.example.contest.entities.EventEntry;
import com.example.contest.repositories.EventEntryRepository;
import com.example.contest.requests.EventEntryRequest;
import com.example.contest.responses.EventEntryResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RequestMapping(
	value = "/v1/event/{eventId}/event-entry",
	produces = MediaType.APPLICATION_JSON_VALUE
)
@RestController
public class EventEntryController {
	private EventEntryRepository eventEntryRepository;

	@Autowired
	public void AuthKeyController(EventEntryRepository eventEntryRepository) {
		this.eventEntryRepository = eventEntryRepository;
	}

	@GetMapping()
	public List<EventEntryResponse> getAll(@PathVariable int eventId) {
		List<Sort.Order> orders = new ArrayList<Sort.Order>();

		Sort.Order order1 = new Sort.Order(Sort.Direction.ASC, "userName");
		orders.add(order1);

		List<EventEntry> eventEntryList = this.eventEntryRepository.findAll(Sort.by(orders));

		return eventEntryList
				.stream()
				.map(this::convertEventEntryToEventEntryResponse)
				.collect(Collectors.toList());
	}

	@DeleteMapping("/{eventEntryId}")
	public String delete(@PathVariable int eventId, @PathVariable long eventEntryId) {
		this.eventEntryRepository.deleteById(eventEntryId);
		return "{ deleted: true }";
	}

	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public String post(@PathVariable int eventId, @RequestBody EventEntryRequest eventEntryRequest) {
		EventEntry newEventEntry = new EventEntry();
		fillEventEntryFromRequest(eventEntryRequest, newEventEntry);

		this.eventEntryRepository.save(newEventEntry);
		return "{ created: true }";
	}

	@PutMapping("/{id}")
	public String put(@PathVariable long id, @RequestBody EventEntryRequest request) {
		EventEntry entry = this.eventEntryRepository.getReferenceById(id);
		fillEventEntryFromRequest(request, entry);

		return "{ updated: true }";
	}


	private EventEntry fillEventEntryFromRequest(EventEntryRequest request, EventEntry entry) {
		entry.setUserName(request.getUserName());
		entry.setOrganisation(request.getOrganisation());

		return entry;
	}

	private EventEntryResponse convertEventEntryToEventEntryResponse(EventEntry eventEntry) {
		EventEntryResponse response = new EventEntryResponse();
		response.setId(eventEntry.getId());

		response.setUserName(eventEntry.getUserName());
		response.setOrganisation(eventEntry.getOrganisation());

		return response;
	}
}