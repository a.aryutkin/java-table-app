package com.example.contest.controllers;

import com.example.contest.entities.EventEntry;
import com.example.contest.repositories.EventEntryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@RestController
public class MyController {
	private EventEntryRepository eventEntryRepository;

	@Autowired
	public void AuthKeyController(EventEntryRepository eventEntryRepository) {
		this.eventEntryRepository = eventEntryRepository;
	}

	@GetMapping("/hello")
	public String hello() {
		EventEntry eventEntry = new EventEntry();
		eventEntry.setUserName("Some name");
		eventEntryRepository.save(eventEntry);

		return "Hello World!";
	}
}