package com.example.contest.repositories;

import com.example.contest.entities.EventEntry;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EventEntryRepository extends JpaRepository<EventEntry, Long> { }