'use strict';

import React, { useState, useEffect } from 'react';
import ReactDOM from 'react-dom';
import client from './client';
import EntriesTable from "./EntriesTable";

export default function App() {
    const [state, setState]= useState({
        entries: [],
    });

    useEffect(() => updateEntriesState(), [])

    function updateEntriesState() {
        client({method: 'GET', path: '/v1/event/1/event-entry'}).done(response => {
            setState(prevState => {
                return { ...prevState, entries: response.entity };
            });
        });
    }

    return (
        <>
            <div className="block_wrapper">
                <EntriesTable rows={state.entries} updateEntries={updateEntriesState} />
            </div>
        </>
    );
}

ReactDOM.render(
    <App />,
    document.getElementById('react')
)
