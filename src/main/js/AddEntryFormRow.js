'use strict';

import TableRow from "@mui/material/TableRow";
import TableCell from "@mui/material/TableCell";
import IconButton from "@mui/material/IconButton";
import CheckCircleIcon from '@mui/icons-material/CheckCircle';

const React = require('react');

export default function AddEntryFormRow(props) {
    return (
        <TableRow className="add-entry-row" key={0}>
            <TableCell>-</TableCell>
            <TableCell component="th" scope="row"><input type="text" name="user_name" required/></TableCell>
            <TableCell><input type="text" name="organisation" required/></TableCell>
            <TableCell>
                <IconButton type="submit" color="primary" size="small">
                    <CheckCircleIcon />
                </IconButton>
            </TableCell>
        </TableRow>
    );
}
