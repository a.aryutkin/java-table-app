import * as React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import AddEntryFormRow from "./AddEntryFormRow";
import RemoveCircleOutlineIcon from '@mui/icons-material/RemoveCircleOutline';
import IconButton from "@mui/material/IconButton";

export default function EntriesTable(props) {
    const rows = props.rows;
    let rowsOrder = 1;

    function handleRemoval(e) {
        const button = e.currentTarget;
        const userId = button.getAttribute("userid");

        if (!userId) return;

        const path = '/v1/event/1/event-entry/' + userId;
        fetch(path, {
            method: 'DELETE'
        }).then(() => {
            props.updateEntries();
        });
    }

    function handleSubmit(e) {
        e.preventDefault();

        const form = e.currentTarget;
        const formData = new FormData(form);

        const formJson = Object.fromEntries(formData.entries());
        const response = fetch('/v1/event/1/event-entry', {
            method: form.method,
            body: JSON.stringify(formJson),
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
        });

        response.then(() => {
            form.reset();
            props.updateEntries();
        });
    }

    return (
        <Paper sx={{ width: '100%', overflow: 'hidden' }}>
            <TableContainer sx={{ maxHeight: 750 }}>
                <form className="add_form" method="post" onSubmit={handleSubmit}>
                <Table stickyHeader size="small">
                    <TableHead>
                        <TableRow>
                            <TableCell>№</TableCell>
                            <TableCell>ФИ</TableCell>
                            <TableCell>АТП</TableCell>
                            <TableCell></TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        <>
                            {/* Form to add a new user */}
                            <AddEntryFormRow/>

                            {/* All entries */}
                            {rows.map((row) => (
                                <TableRow
                                    key={row.id}
                                >
                                    <TableCell>{rowsOrder++}</TableCell>
                                    <TableCell component="th" scope="row">{row.user_name}</TableCell>
                                    <TableCell>{row.organisation}</TableCell>

                                    <TableCell>
                                        <IconButton userid={row.id} onClick={handleRemoval} color="primary" size="small">
                                            <RemoveCircleOutlineIcon/>
                                        </IconButton>
                                    </TableCell>
                                </TableRow>
                            ))}
                        </>
                    </TableBody>
                </Table>
                </form>
            </TableContainer>
        </Paper>
    );
}
