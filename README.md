# Приложение таблицы, написанное на java (backend) и react (frontend)

Это упрощенная версия таблицы, которая использовалась в реальном проекте (в полной версии осуществлялись еще необходимые подсчеты). На примере этого приложения хочу показать dockerfile и docker-compose.yml, которые получились в ходе реализации проекта.

## Локальный запуск

Требования: [Git](https://git-scm.com/book/ru/v2/Введение-Установка-Git), Docker и Docker-compose, например через [Docker-desctop](https://docs.docker.com/desktop/).

Склонировать удаленный репозиторий:
```
git clone https://gitlab.com/a.aryutkin/java-table-app.git
```

Перейти в папку с репозиторием:
```
cd java-table-app
```

Запустить docker-compose:
```
docker-compose up -d
```
Первая сборка может занять продолжительное время (~15-20 минут), так как будут скачиваться дополнительные образы и отрабатывать команды. Плюс из-за того, что проект небольшой, файлы для сборки frontend и backend кладутся в одну папку и собираются все вместе.

После того, как контейнеры создадутся можно проверить, что они запущены и работают:
```
docker ps
```

```
CONTAINER ID   IMAGE                    COMMAND                  CREATED          STATUS          PORTS                    NAMES
44ae10520eb9   java-table-app-app       "java org.springfram…"   2 minutes ago   Up 2 minutes   0.0.0.0:8080->8080/tcp   app
705d591bfe7f   postgres:15-alpine3.17   "docker-entrypoint.s…"   2 minutes ago   Up 2 minutes   0.0.0.0:5432->5432/tcp   postgres-db
```

Перейти на localhost, чтобы увидеть приложение и его пощупать:
```
http://localhost:8080
```

Будет 3 столбца с номером строки, ФИ и АТП, можно вводить данные и фиксировать нажатием на галочку. Также можно удалять строки нажатием на минус.

После окончания работ с приложением запускаем команду остановки docker-compose:
```
docker-compose down
```

## Примечания

Стадии сборки JDK и запуска .jar в dockerfile были сделаны на основе [статьи](https://struchkov.dev/blog/ru/build-docker-image-for-spring-boot/).

Если вдруг возникнет необходимость пересобрать и запустить на новом образе, то нужно запустить скрипт:
```
./local-run.sh
```
Сборка будет проходить быстрее из-за кешированных слоев.
