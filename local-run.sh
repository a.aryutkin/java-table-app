#!/bin/bash

docker image build . -t java-table-app-app # (rebuild image)

docker-compose up -d # (run containers)
